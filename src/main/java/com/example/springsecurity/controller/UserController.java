package com.example.springsecurity.controller;

import com.example.springsecurity.domain.dto.UserDto;
import com.example.springsecurity.service.UserDetailsServiceImpl;
import com.example.springsecurity.service.UserDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserDetailsServiceImpl userDetailsService;
    private final UserDtoMapper userDtoMapper;
    // For basic auth
    @GetMapping("/")
    public String basicAuth(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();

        model.addAttribute("employees", new String[]{"ddff"});
        printSecurityUserName();

        return "redirect:/dashboard";
    }

    private String printSecurityUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        System.out.println(name);


        return name;
    }

    // For user details auth
    @GetMapping("/login")
    public String userDetails(Model model) {
        model.addAttribute("employees", new String[]{"ddff"});

        return "/login";
    }

    @GetMapping("/index")
    public String index(Model model) {

        return "/index";
    }

    @GetMapping("/register")
    public String reg(){
        return "register";
    }

    @PostMapping("/register")
    public String redirectToLogin(@RequestParam String username, @RequestParam String password) {
                Boolean isSuccess = userDetailsService.createUser(username, password);
                if(isSuccess){
                        return "login";
                    }
                return "login";
    }
    @GetMapping("/dashboard")
    public String dashboardGet(){
        return "dashboard";
    }

    @PostMapping("/dashboard")
    public String dashboard(){
        return "dashboard";
    }

}
